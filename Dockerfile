FROM maven-wildfly:latest 
COPY node-info /usr/src/node-info 
USER root
RUN chown -R jboss /usr/src/node-info 
RUN mvn -f /usr/src/node-info/pom.xml clean package ; cp /usr/src/nodeinfo/target/node-info.war /opt/jboss/wildfly/standalone/deployments/node-info.war
USER jboss 
CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-b", "0.0.0.0", "bmanagement", "0.0.0.0"] 
